/// @file
/// @brief File that contains functions to work with PE files 
#include "../include/pe.h"
#include <malloc.h>
#include <string.h>

/// Size of the buffer
#define BUFFER_SIZE (4096 * 2)

/// @brief Reads information from input file to the PEFile structure 
/// @param in Specifies the file to read from
/// @param pe_file Specifies pointer to the PEFile structure to read information to
/// @return One of the possible values specified enum (now only OK or WRONG)
enum read_result read_pe(FILE* in, struct PEFile* pe_file){
    /// Go to the default offset
    int seek_res = fseek(in, DEFAULT_OFFSET, SEEK_SET);
    if (seek_res) return READ_WRONG;
    /// Read real offset
    size_t res = fread(&pe_file->magic_offset, 1, sizeof(pe_file->magic_offset), in);
    if (res != sizeof(pe_file->magic_offset)) return READ_WRONG;

    /// Go to the real offset
    seek_res = fseek(in, pe_file->magic_offset, SEEK_SET);
    if (seek_res) return READ_WRONG;
    /// Read file magic
    res = fread(&pe_file->magic, 1, sizeof(pe_file->magic), in);
    if (res != sizeof(pe_file->magic)) return READ_WRONG;

    /// Read PEHeader
    res = fread(&pe_file->header, 1, sizeof(struct PEHeader), in);
    if (res != sizeof(struct PEHeader)) return READ_WRONG;
    // printf("Optional header size: %d\n", pe_file->header.size_of_optional_header);
    // printf("Number of sections: %d\n", pe_file->header.number_of_sections);

    pe_file->section_headers = malloc(sizeof(struct SectionHeader) * pe_file->header.number_of_sections);

    /// Read OptionalHeader.magic
    res = fread(&pe_file->optional_header.magic, 1, sizeof(struct OptionalHeader), in);
    if (res != sizeof(struct OptionalHeader)) return READ_WRONG;
    // printf("Optional header magic is: %x\n", pe_file->optional_header.magic);

    /// Read OptionalHeader depending on file version
    if (pe_file->optional_header.magic == PE32_MAGIC) {
        res = fread(&pe_file->optional_header.optional_header_pe32, 1, sizeof(struct OptionalHeaderPE32), in);
        if (res != sizeof(struct OptionalHeaderPE32)) return READ_WRONG;
    } else if (pe_file->optional_header.magic == PE32_PLUS_MAGIC){
        res = fread(&pe_file->optional_header.optinal_header_pe32_plus, 1, sizeof(struct OptionalHeaderPE32Plus), in);
        if (res != sizeof(struct OptionalHeaderPE32Plus)) return READ_WRONG;
    } else return READ_WRONG;

    /// Calculate offset to first SectionHeader
    pe_file->section_header_offset = pe_file->magic_offset + sizeof(pe_file->magic) + sizeof(struct PEHeader) + pe_file->header.size_of_optional_header;
    
    /// Go to the first SectionHeader
    seek_res = fseek(in, pe_file->section_header_offset, SEEK_SET);
    if (seek_res) return READ_WRONG;
    /// Read all the SectionHeaders 
    for (uint16_t i = 0; i < pe_file->header.number_of_sections; ++i){
        res = fread(&pe_file->section_headers[i], 1, sizeof(struct SectionHeader), in);
        if (res != sizeof(struct SectionHeader)) return READ_WRONG;
        // printf("Cur section name: %s\n", pe_file->section_headers[i].name);
    }
    // printf("Sizeof section header: %lu\n", sizeof(struct SectionHeader));
    return READ_OK;
}


/// @brief Returns pointer to the SectionHeader specified by 'section'
/// @param pe_file PEFile structure in which to look for the section
/// @param section Name of the section to find
/// @return Pointer to SectionHeader if found, NULL else
struct SectionHeader* get_section_by_name(struct PEFile* pe_file, const char* section){
    for (size_t i = 0; i < pe_file->header.number_of_sections; ++i) {
        if (strcmp(pe_file->section_headers[i].name, section) == 0) return pe_file->section_headers + i;
    }
    return NULL;
}

/// @brief Copies the specified section from input file to the output file
/// @param in PE file needed to read the section
/// @param out File that section is copied to 
/// @param pe_file PEFile structure pointer that contains information about SectionHeaders
/// @param section Name of the section to copy
/// @return One of the possible values specified in enum (now only OK and WRONG)
enum write_result write_section(FILE* in, FILE* out, struct PEFile* pe_file, const char* section){
    /// get pointer to SectionHeader if it exists in file 
    struct SectionHeader* section_ptr = get_section_by_name(pe_file, section);
    if (!section_ptr) return WRITE_WRONG;

    /// Go to the start of the section data
    int seek_res = fseek(in, section_ptr->pointer_to_raw_data, SEEK_SET);
    if (seek_res) return WRITE_WRONG;

    uint8_t buffer[BUFFER_SIZE];

    size_t left_data_size = section_ptr->size_of_raw_data;
    size_t res;
    /// Copy the data by batches of size: BUFFER_SIZE
    for (;;)
    {
        if (left_data_size >= BUFFER_SIZE) {
            res = fread(buffer, 1, BUFFER_SIZE, in);
            if (res != BUFFER_SIZE) return WRITE_WRONG;
            res = fwrite(buffer, 1, BUFFER_SIZE, out);
            if (res != BUFFER_SIZE) return WRITE_WRONG;
        } else {
            res = fread(buffer, 1, left_data_size, in);
            if (res != left_data_size) return WRITE_WRONG;
            res = fwrite(buffer, 1, left_data_size, out);
            if (res != left_data_size) return WRITE_WRONG;
            return WRITE_OK;
        }
        left_data_size -= BUFFER_SIZE;
    }
}


/// @brief Frees data in PEFile structure (now only array of SectionHeader)
/// @param pe_file Structure to be freed
void free_pe_file(struct PEFile* pe_file){
    free(pe_file->section_headers);
}
