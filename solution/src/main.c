/// @file 
/// @brief Main application file

#include "../include/io.h"
#include "../include/pe.h"

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  /// Exit if number of arguments not equal to 4 and prints correct way to use of program
  if (argc != 4) fatal("Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
  /// Open input file
  FILE* file_in = fopen(argv[1], "r");
  if (!file_in) fatal("Input file can not be opened.\n");
  /// Open output file
  FILE* file_out = fopen(argv[3], "wb");
  if (!file_out){
    fclose(file_in);
    fatal("Output file can not be opened.\n");
  }

  /// Read data from input file to PEFile structure
  struct PEFile pe_file;
  enum read_result read_res = read_pe(file_in, &pe_file);
  
  if (read_res == READ_WRONG){
    fclose(file_in);
    fclose(file_out);
    fatal("Corrupted data in input file.\n");  
  }

  /// Copy section from one file to another
  enum write_result write_res = write_section(file_in, file_out, &pe_file, argv[2]);

  /// Close files and free memory
  free_pe_file(&pe_file);
  fclose(file_out);
  fclose(file_in);

  if (write_res == WRITE_WRONG) fatal("Can not write section to output file.\n");
  printf("Successfully copied section.\n");
  return 0;
}
