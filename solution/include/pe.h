
/// @file 
/// @brief Contains information about internal structure of PE file splitted into several structures
#include <stdint.h>
#include <stdio.h>

#pragma once

#ifndef PE_HEADER
#define PE_HEADER

/// Offset to the real offset
#define DEFAULT_OFFSET 0x3c
/// Magic number for PE32 files
#define PE32_MAGIC 0x10b
/// Magic number for PE32+ files
#define PE32_PLUS_MAGIC 0x20b

/// Structure containing Main header
#ifdef _MSC_VER
#pragma pack(1)
#endif
struct
#ifdef __GNUC__
__attribute__((packed))
#endif
PEHeader
{
    /// The number that identifies the type of target machine
    uint16_t machine;
    /// The number of sections
    uint16_t number_of_sections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    uint32_t time_date_stamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t pointer_to_symbol_table;
    /// The number of entries in the symbol table
    uint32_t number_of_symbols;
    /// The size of the optional header
    uint16_t size_of_optional_header;
    /// The flags that indicate the attributes of the file
    uint16_t characteristics;
};
#ifdef _MSC_VER
#pragma pop()
#endif

/// Structure containing Optional header for PE32 executable
#ifdef _MSC_VER
#pragma pack(1)
#endif
struct
#ifdef __GNUC__
__attribute__((packed)) 
#endif
OptionalHeaderPE32
{
    /// @name Standard fields
    /// @{
    
    /// The linker major version number
    uint8_t major_linker_version;
    /// The linker minor version number
    uint8_t minor_linker_version;
    /// The size of the code section
    uint32_t size_of_code;
    /// The size of the initialized data section
    uint32_t size_of_initialized_data;
    /// The size of the uninitialized data section
    uint32_t size_of_unitialized_data;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t address_of_entry_point;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t base_of_code;
    /// The address that is relative to the image base of the beginning-of-data section when it is loaded into memory
    uint32_t base_of_data;
    /// @}
};
#ifdef _MSC_VER
#pragma pop()
#endif


/// Structure containing Optional header for PE32+ executable
#ifdef _MSC_VER
#pragma pack(1)
#endif
struct 
#ifdef __GNUC__
__attribute__((packed))
#endif 
OptionalHeaderPE32Plus
{
    /// @name Standard fields
    /// @{
    
    /// The linker major version number
    uint8_t major_linker_version;
    /// The linker minor version number
    uint8_t minor_linker_version;
    /// The size of the code section
    uint32_t size_of_code;
    /// The size of the initialized data section
    uint32_t size_of_initialized_data;
    /// The size of the uninitialized data section
    uint32_t size_of_unitialized_data;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t address_of_entry_point;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t base_of_code;
    /// @}
};
#ifdef _MSC_VER
#pragma pop()
#endif

/// Structure containing Optional header information with 2 different realisations (PE32 and PE32+)
struct OptionalHeader
{
    /// Optional header magic - 0x10b for PE32 or 0x20b for PE32+
    uint16_t magic;
    /// Optional header for PE32 executables
    struct OptionalHeaderPE32 optional_header_pe32;
    /// Optional header for PE32+ executables
    struct OptionalHeaderPE32Plus optinal_header_pe32_plus;
};

/// Structure containing one Section header data
#ifdef _MSC_VER
#pragma pack(1)
#endif
struct 
#ifdef __GNUC__
__attribute__((packed))
#endif 
SectionHeader
{
    /// An 8-byte, null-padded UTF-8 encoded string
    char name[8];
    /// The total size of the section when loaded into memory
    uint32_t virtual_size;
    /// This field is the address of the first byte before relocation is applied
    uint32_t virtual_address;
    /// The size of the section
    uint32_t size_of_raw_data;
    /// The file pointer to the first page of the section within the COFF file
    uint32_t pointer_to_raw_data;
    /// The file pointer to the beginning of relocation entries for the section
    uint32_t pointer_to_relocations;
    /// The file pointer to the beginning of line-number entries for the section
    uint32_t pointer_to_linenumbers;
    /// The number of relocation entries for the section
    uint16_t number_of_relocations;
    /// The number of line-number entries for the section
    uint16_t number_of_linenumbers;
    /// The flags that describe the characteristics of the section
    uint32_t characteristics;
};
#ifdef _MSC_VER
#pragma pop()
#endif

/// Structure containing PE file data
struct PEFile
{
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    /// @{

    ///File magic
    uint32_t magic;
    ///Main header
    struct PEHeader header;
    /// Optional header
    struct OptionalHeader optional_header;
    /// Array of section headers with size of header.number_of_sections
    struct SectionHeader* section_headers;
    ///@}
};

/// Enum containing possible read_pe functions results
enum read_result{
    READ_OK,
    READ_WRONG
};

/// Function that reads information from file to PEFile structure 
enum read_result read_pe(FILE* in, struct PEFile* pe_file);

/// Enum containing possible write_section functions results
enum write_result{
    WRITE_OK,
    WRITE_WRONG
};

/// Function that write specified section into the output file
enum write_result write_section(FILE* in, FILE* out, struct PEFile* pe_file, const char* section);

/// Function that frees memory allocated for SectionHeaders array inside PEFile structure
void free_pe_file(struct PEFile* pe_file);

#endif
