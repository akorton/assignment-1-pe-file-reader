/// @file
/// @brief Header file for input/output functions
#pragma once

#ifndef MY_IO
#define MY_IO
/// @brief Ends the program with an error
_Noreturn void fatal( const char* msg, ... );
#endif
